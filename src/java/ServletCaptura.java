	/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import java.io.File;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Properties;
//import javax.xml.namespace.QName;
/**
 *
 * @author mcama
 */
public class ServletCaptura extends HttpServlet {

	static PrintWriter out;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		// sleep para simular proceso del afis // borrar
		try {
			Thread.sleep(2000);
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
		
		
		String sWSQi = null;
		String sBMPi = null;
		String sANSIi = null;
		String sPKSagemi = null;
		String sWSQd = null;
		String sBMPd = null;
		String sANSId = null;
		String sPKSagemd = null;
		
		byte[] bWSQi = null;
		byte[] bBMPi = null;
		byte[] bANSIi = null;
		byte[] bPKSagemi = null;
		byte[] bWSQd = null;
		byte[] bBMPd = null;
		byte[] bANSId = null;
		byte[] bPKSagemd = null;
		
//		String dni = "";

		//Adquisicion de los valores
		sWSQi = request.getParameter("wsq_Izq");
		System.out.println("sWSQ=");
		sBMPi = request.getParameter("bmp_Izq");
		System.out.println("sBMP=");
		sANSIi = request.getParameter("ansi_Izq");
		System.out.println("sANSI=");
		sPKSagemi = request.getParameter("pkSagem_Izq");
		System.out.println("sPKSagem=");
		
		sWSQd = request.getParameter("wsq_Der");
		System.out.println("sWSQ=");
		sBMPd = request.getParameter("bmp_Der");
		System.out.println("sBMP=");
		sANSId = request.getParameter("ansi_Der");
		System.out.println("sANSI=");
		sPKSagemd = request.getParameter("pkSagem_Der");
		System.out.println("sPKSagem=");
		
		//request.getParameterValues("amputados[]");		
		
//		if(sWSQi.isEmpty() || sBMPi.isEmpty() || sANSIi.isEmpty() || sPKSagemi.isEmpty()){
//			this.lanzarError("Error: Los parametros de la huella izquierda son incorrectos.", response);
//			return;
//		}
//		if(sWSQd.isEmpty() || sBMPd.isEmpty() || sANSId.isEmpty() || sPKSagemd.isEmpty()){
//			this.lanzarError("Error: Los parametros de la huella derecha son incorrectos.", response);
//			return;
//		}
		
		
		
//        dni = request.getParameter("dni");
//        System.out.println("dni=" + dni);

		bWSQi = new sun.misc.BASE64Decoder().decodeBuffer(sWSQi);
		System.out.println("len WSQ = " + bWSQi.length);
		bBMPi = new sun.misc.BASE64Decoder().decodeBuffer(sBMPi);
		System.out.println("len BMP = " + bBMPi.length);
		bANSIi = new sun.misc.BASE64Decoder().decodeBuffer(sANSIi);
		System.out.println("len ANSI = " + bANSIi.length);
		bPKSagemi = new sun.misc.BASE64Decoder().decodeBuffer(sPKSagemi);
		System.out.println("len PKSagem = " + bPKSagemi.length);
		
		
		bWSQd = new sun.misc.BASE64Decoder().decodeBuffer(sWSQd);
		System.out.println("len WSQ = " + bWSQi.length);
		bBMPd = new sun.misc.BASE64Decoder().decodeBuffer(sBMPd);
		System.out.println("len BMP = " + bBMPi.length);
		bANSId = new sun.misc.BASE64Decoder().decodeBuffer(sANSIi);
		System.out.println("len ANSI = " + bANSIi.length);
		bPKSagemd = new sun.misc.BASE64Decoder().decodeBuffer(sPKSagemd);
		System.out.println("len PKSagem = " + bPKSagemi.length);

		
		//out = response.getWriter();
		//response.getWriter().write("{error:1,mensaje:'respuesta json'}");
		//out.print("{error:1,mensaje:'respuesta json'}");
		
		// mostrar coincidencias
		Map salida = new HashMap();
		salida.put("error", false);
		
		List coins = new ArrayList();
		Map coin1 = new HashMap(), coin2 = new HashMap();
		
		coin1.put("apellido_paterno", "Flores");
		coin1.put("apellido_materno", "Belizario");
		coin1.put("prenombres", "Victor Dino");
		coin1.put("nacimiento", "15/12/1986");
		coin1.put("sexo", "M");
		coin1.put("estado_civil", "Casado");
		coin1.put("foto", "dino.png");
		coins.add(coin1);
		
		coin2.put("apellido_paterno", "Mamani");
		coin2.put("apellido_materno", "Humpiri");
		coin2.put("prenombres", "Luis Miguel");
		coin2.put("nacimiento", "15/12/1986");
		coin2.put("sexo", "M");
		coin2.put("estado_civil", "soltero");
		coin2.put("foto", "foto.png");
		coins.add(coin2);
		
//		salida.put("coincidencias", "[List de coincidencias]");
		salida.put("lista", coins);
		this.lanzarJson(salida, response);
		
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
////		response.getWriter().write("{error:1,mensaje:'respuesta json'}");		
//		response.getWriter().write(new Gson().toJson(salida));
		
//		try {
//
//			// TODO output your page here
//			out.println("<html>");
//			out.println("<head>");
//			out.println("<title>Servlet ServletCaptura</title>");
//			out.println("</head>");
//			out.println("<body>");
//			out.println("<h1>Servlet ServletCaptura at " + request.getContextPath() + "</h1>");
//			out.println("<br><br>");
//			//initMain();
//
//			out.println("<br><br>");
//			out.println("</body>");
//			out.println("</html>");
//			//
//		} finally {
//			out.close();
//		}
	}
	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

	/**
	 * Handles the HTTP
	 * <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP
	 * <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
	
	protected void lanzarError(String mensaje, HttpServletResponse res) throws IOException{
		Map json = new HashMap();
		json.put("error", true);
		json.put("mensaje", mensaje);
		this.lanzarJson(json, res);
	}
	
	protected void lanzarJson(Map json, HttpServletResponse res) throws IOException{
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");	
		res.getWriter().write(new Gson().toJson(json));		
	}
}
