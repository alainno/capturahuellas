<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Capturador de Huellas</title>
        <meta name="description" content="Capturador de Huellas">
        <meta name="viewport" content="width=device-width">
		<link rel="icon" type="image/vnd.microsoft.icon" href="img/favicon.ico" />
		<link rel="SHORTCUT ICON" href="img/favicon.ico" />
		<!-- estilos -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/tipss.css">
		<link rel="stylesheet" href="css/captura.css">
		<!-- compatibilidad html5 para IE -->
        <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

		<div class="envoltura">

			<header class="clearer">
				<ul class="tabs left">
					<li><a href="#paso1" class="actual"><span class="left">1. Huella Izquierda</span><em class="triangle"></em></a></li>
					<li><a href="#paso2"><span class="left">2. Huella Derecha</span><em class="triangle"></em></a></li>
					<li><a href="#paso3"><span class="left">3. Resumen y Enviar</span><em class="triangle"></em></a></li>
					<li><a href="#paso4"><span class="left">4. Resultados</span><em class="triangle"></em></a></li>

				</ul>
				<button type="button" class="btn btn-warning right" id="btn-cancelar" ><i class="icon-repeat icon-white"></i> Reiniciar</button>
			</header>

			<form method="post" action="${pageContext.request.contextPath}/ServletCaptura" id="form-huellas">
				<!-- nro de huellas requeridas -->
				<input type="hidden" name="nro_huellas" value="" />
				
				<div class="mt10 contenedores">

					<section id="paso1">

						<div class="caja">
							<h1>Paso 1: Capturar Huella Izquierda</h1>

							<div class="alert alert-yellow mt10">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/alert.png" class="left" />
									<p>Presione el botón para capturar la huella.</p>
								</div>
							</div>
							<div class="alert alert-error mt10 oculto">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/error.png" class="left" />
									<p></p>
								</div>
							</div>
							<div class="alert alert-success mt10 oculto">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/ok.png" class="left" />
									<p>Presione el botón para capturar la huella.</p>
								</div>
							</div>
							<input type="hidden" name="tpl[]" />
						</div>

						<div class="caja mt10">
							<div class="clearer">

								<div class="left form-inline">
									<button type="button" class="btn btn-large btn-captura btn-primary">Capturar</button>

									<label class="checkbox ml20">
										<input type="checkbox" name="amputado[]" value="1" />
										No presenta (amputado).
									</label>						
								</div>

								<button type="button" class="btn btn-large right btn-siguiente">Siguiente <i class="icon-chevron-right"></i></button>

							</div>									
						</div>

					</section>

					<section id="paso2" class="oculto">
						<div class="caja">
							<h1>Paso 2: Capturar Huella Derecha</h1>
							<div class="alert alert-yellow mt10">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/alert.png" class="left" />
									<p>Presione el botón para capturar la huella.</p>
								</div>
							</div>
							<div class="alert alert-error mt10 oculto">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/error.png" class="left" />
									<p></p>
								</div>
							</div>
							<div class="alert alert-success mt10 oculto">
								<div class="clearer">
									<img src="${pageContext.request.contextPath}/img/ok.png" class="left" />
									<p>Presione el botón para capturar la huella.</p>
								</div>
							</div>
							<input type="hidden" name="tpl[]" />
						</div>						

						<div class="caja mt10">
							<div class="clearer">

								<div class="left form-inline">
									<button type="button" class="btn btn-large btn-captura btn-primary">Capturar</button>

									<label class="checkbox ml20">
										<input type="checkbox" name="amputado[]" value="1" />
										No presenta (amputado).
									</label>						
								</div>

								<button type="button" class="btn btn-large right btn-siguiente">Siguiente <i class="icon-chevron-right"></i></button>

							</div>
						</div>

					</section>

					<section id="paso3" class="oculto">
						<div class="caja">
							<h1>Paso 3: Resumen y Enviar</h1>
							<div class="alert oculto alert-error mt5"></div>
							<!--<div class="container">-->
							<table class="table-bordered table" id="tbl-resumen">
							</table>
							<!--</div>-->
						</div>
						<div class="caja mt10">
							<div class="clearer">



								<button type="button" class="btn btn-large btn-primary right btn-siguiente">Enviar <i class="icon-chevron-right icon-white"></i></button>


							</div>							
						</div>

					</section>

					<section id="paso4" class="oculto">
						<div class="caja">
							<h1>Paso 4: Resultados</h1>
							<table class="table-bordered table" id="tabla-coins">
								<tr>
									<th>Nombres y Apellidos</th>
									<th>Detalles</th>
								</tr>
							</table>
							<div class="datos mt10">
								<article class="oculto curva">
									<img src="img/foto.png" class="right foto" />
									<table>
										<tr>
											<th>Primer Apellido:</th>
											<td></td>
										</tr>
										<tr>
											<th>Segundo Apellido:</th>
											<td></td>
										</tr>
										<tr>
											<th>Pre Nombres:</th>
											<td></td>
										</tr>
										<tr>
											<th>Nacimiendo:</th>
											<td></td>
										</tr>
										<tr>
											<th>Sexo:</th>
											<td></td>
										</tr>
										<tr>
											<th>Estado Civil:</th>
											<td></td>
										</tr>
									</table>
								</article>
								<article class="oculto curva">
									<img src="img/foto.png" class="right foto" />
									<table>
										<tr>
											<th>Primer Apellido:</th>
											<td></td>
										</tr>
										<tr>
											<th>Segundo Apellido:</th>
											<td></td>
										</tr>
										<tr>
											<th>Pre Nombres:</th>
											<td></td>
										</tr>
										<tr>
											<th>Nacimiendo:</th>
											<td></td>
										</tr>
										<tr>
											<th>Sexo:</th>
											<td></td>
										</tr>
										<tr>
											<th>Estado Civil:</th>
											<td></td>
										</tr>
									</table>
								</article>
							</div>
						</div>
					</section>



				</div>

				<input type="hidden" name="bmp_Izq"/>
				<input type="hidden" name="wsq_Izq"/>
				<input type="hidden" name="ansi_Izq"/>
				<input type="hidden" name="pkSagem_Izq"/>
				<input type="hidden" name="coCap_Izq"/>

				<input type="hidden" name="bmp_Der"/>
				<input type="hidden" name="wsq_Der"/>
				<input type="hidden" name="ansi_Der"/>
				<input type="hidden" name="pkSagem_Der"/>
				<input type="hidden" name="coCap_Der"/>

			</form>


		</div>

		<applet name="capturaSuport" id="capturaSuport" code="pe.gob.reniec.captura.CapturaApplet" CODEBASE="${pageContext.request.contextPath}/applet" archive="appletCaptura.jar, lib/log4j-1.2.15.jar" width="5" height="5">captura</applet>

    </body>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/plugins/vendor/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="js/plugins/bootstrap.min.js"></script>
	<script src="js/plugins/plugins.js"></script>
	<script src="js/captura.js"></script>
</html>
