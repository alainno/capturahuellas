// variables de configuración
var huellas_requeridas = 1; // ó 2
var tiempo_envios = 30; // en segundos
var max_capturas = 10;

// Manejo de eventos
//$(document).ready(mainCaptura);

// id del timer
var idActualizarEnvios = null;

function mainCaptura() {

	console.log('iniciando script de captura....');

	// obtenemos nro de huellas requeridas desde jsp
	var nro_huellas = $(':hidden[name="nro_huellas"]').val();
	huellas_requeridas = nro_huellas ? nro_huellas : huellas_requeridas;

	// todos los eventos
    $('.tabs a').live('click', function(){return false;})
	// check amputado
	$(':checkbox[name="amputado[]"]').live('click', marcarAmputado);
	// click boton capturar
	$('.btn-captura').live('click', capturarHuella);
	// click boton siguiente
	$('.btn-siguiente').live('click', continuarPaso);
	// click boton reiniciar
//	$('#btn-cancelar').live('click', reiniciarProceso);
	// click boton ver ciudadano
//	$('.lnk-ver').live('click', verCiudadano);

    // cronogramar envios
//    actualizarEnvios();
//    idActualizarEnvios = setInterval(actualizarEnvios, tiempo_envios*1000);

    // toggle lista de procesos
    $('div.lista-bottom header #lnk-toggle').live('click', toggleListaCapturas);

//    $('#btn-denuevo').live('click', reiniciarProceso);

    $('a.lnk-ver-candidatos').live('click',verCandidatos);

    $('select#candidatos').live('change',seleccionarCandidato);

    $('#lnk-clear').live('click', limpiarListaEnvios);
}

function limpiarListaEnvios(){
    var nro_tds = $('#envios table tr:eq(1) td').length;
    if(nro_tds > 1){
        if(confirm('¿Desea eliminar todos los procesos?')){
            reiniciarProceso();
        }
    }
    else{
        alert('No hay procesos para eliminar.');
    }
    return false;
}

function seleccionarCandidato(){
    var $este = $(this);
    $este.parents('#dlg-ver').find('article').hide();
    $este.parents('#dlg-ver').find('article[id="card-'+$este.val()+'"]').show();
}

function verCandidatos(e){
    e.preventDefault();
    var $lnk = $(this);
    $('.super-envoltura').ajaxui('bloquear');
    $.getJSON($lnk.attr('href'), function(json){
        $('.super-envoltura').ajaxui('desbloquear');
        if(json == null){
            alert('No se obtuvo respuesta del servidor');
        }
        else{
            if(typeof json.listaConsultas != 'undefined'){
                var $dlg = $('#dlg-ver');
                var $select_candi = $dlg.find('#candidatos');
                $select_candi.find('option').remove();
                var nro_candis = 0;
                $.each(json['listaConsultas'], function(k,v){
                    $select_candi.append('<option value="'+ v.nuDni+'">DNI: '+ v.nuDni+'</option>');
                    nro_candis++;
                });
                var $cards = $dlg.find('div.datos');
                var $cards_tpl = $dlg.find('#tpl-datos');
                var template = Mustache.render($cards_tpl.html(), json);
                $cards.html('').html(template);
                $select_candi.find('option:eq(0)').select();
                $cards.find('article:eq(0)').show();
                $dlg.dialog({
                    modal:true
                    ,width:623//'auto'
                    ,resizable:false
                    ,title:nro_candis==1?'(' + nro_candis + ') candidato':'(' + nro_candis + ') candidatos'
                    ,close:function(){$(this).dialog('destroy')}
                    ,buttons:{'Cerrar':function(){$(this).dialog('destroy')}}
                });
            }
            else{
                alert('No se obtuvo la lista de candidatos.');
            }
        }
    });
}

function toggleListaCapturas(){
    var $wrapcola = $('#envios');
    if($wrapcola.is(':visible')){
        $('div.lista-bottom').addClass('cerrado');
        $('div.capa-central').addClass('extendido');
        $(this).children('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');
    }
    else{
        $('div.lista-bottom').removeClass('cerrado');
        $('div.capa-central').removeClass('extendido');
        $(this).children('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
    }
    $wrapcola.toggle();
}

function actualizarEnvios(){
    var $container = $('#envios');
    var $tabla = $container.find('table');

    if($container.length > 0){
        var url = $container.attr('rel');
        $.post(url, {}, function(json){
           $tabla.find("tr:gt(0)").remove();
           if(json == null || typeof json.listaRegistroAfis == 'undefined'){
               $tabla.append('<tr><td colspan="6">No se encontraron procesos de captura.</td></tr>');
           }
            else{
               llenarTablaEnvios(json['listaRegistroAfis'],$tabla);
           }
        }, 'json');
    }
}

function marcarAmputado() {
	// deshabilitar / habilitar boton capturar
	var $check = $(this);
	var $btn_captura = $check.parents('section').find('.btn-captura');
	if ($check.is(':checked')) {
		$btn_captura.attr('disabled', 'disabled');
	}
	else {
		$btn_captura.removeAttr('disabled');
	}
}

function capturarHuella() {

	console.log('iniciando captura de huella...');

    var $boton = $(this);
	var $form = $("#form-huellas");
	var index = $('.tabs li a.actual').parent('li').index();
	var $seccion = $('.contenedores section').eq(index);
	var $input = $seccion.find(':hidden[name="tpl[]"]');
	var formulario = $form[0];
	var exito = false;

    $input.val('');
    $form.ajaxui('bloquear');
    $form.jsForm('disable');

    //setTimeout(function(index){
        switch (index) {
            case 0:
                exito = fn_capHuellaIzqN(formulario);
                break;
            case 1:
                exito = fn_capHuellaDerN(formulario);
                break;
        }
    //}, 1);

	$form.jsForm('enable');
	$form.ajaxui('desbloquear');

	$seccion.find('.alert:visible').hide();

	if (!exito) {
		$seccion.find('.alert-yellow').fadeIn();
		$boton.text('Capturar');
	}
	else {
		$input.val(1);
		$boton.text('Re-capturar');
		$seccion.find('.alert-success').fadeIn().find('p').text('La huella fue capturada con éxito.');
	}
}

function continuarPaso() {
	var index = $('.tabs li a.actual').parent('li').index();
	var index_siguiente = index + 1;

	// antes validar
	if (validar(index)){
		$('.tabs li').eq(index).children('a').addClass('pasado');
		$('.tabs li a').eq(index_siguiente).gsTab(function(){
			iniciarTab(index_siguiente);
		});
	}
    return false;
}

function iniciarTab(index){
	switch(index){
		case 2:
			var captions = ['Huella Izquierda', 'Huella Derecha'];
			var $tabla = $('#tbl-resumen');

			$tabla.html('');

			for (var i = 0; i < 2; i++) {
				var $amputado = $(':checkbox[name="amputado[]"]').eq(i)
					, $tpl = $(':hidden[name="tpl[]"]').eq(i);

				var color = '', valor = '';

				if ($amputado.prop('checked')) {
					color = 'rojo';
					valor = 'No presenta (amputado).';
				}
				else if ($tpl.val() !== '') {
					color = 'verde';
					valor = 'Huella Capturada.';
				}
				else {
					alert('Error imposible.');
				}
				$tabla.append('<tr><td><strong>' + captions[i] + '</strong></td><td><span class="' + color + '">' + valor + '</span></td></tr>');
			}
		break;
        default:
	}
}

function validar(index) {

	var $seccion = $('.contenedores section').eq(index);

	// validar captura de huellas
	if (index === 0 || index === 1) {
		var tpl = $(':hidden[name="tpl[]"]').eq(index).val();
		var amputado = $(':checkbox[name="amputado[]"]').eq(index).prop('checked');
		if (tpl === '' && !amputado) {

			$seccion.find('.alert:visible').hide();
			$seccion.find('.alert-error').fadeIn().find('p').text('Debe capturar la huella o marcarla como amputado.');
			return false;
		}
	}
	else if (index === 2) { // validar envio
		var $amputados = $(':checked[name="amputado[]"]');
//		var $capturas = $(':hidden[name="tpl[]"]');
//		if ($capturas.eq(0).val() === '' && $capturas.eq(1).val() === '') {
//			$('.alert-error').text('Error: Debe capturar al menos una huella.').slideDown('slow');
//			return false;
//		}
		var cont_huellas = 0;

		if (!$amputados.eq(0).prop('checked')){
			cont_huellas++;
		}
		if(!$amputados.eq(1).prop('checked')) {	
			cont_huellas++;
		}

		if(huellas_requeridas > cont_huellas){
            var mensaje_error = '';
			switch(huellas_requeridas){
				case 1:
					mensaje_error = 'Debe capturar al menos una huella.';
				break;
				case 2:
					mensaje_error = 'Debe capturar las dos huellas.';
				break;
				default:
					mensaje_error = 'Nro. de huellas requeridas incorrecto.';
			}
            $seccion.find('.alert-error').text('Error: ' + mensaje_error).slideDown('slow');
			return;
		}

        // iniciar envio al servidor
        clearInterval(idActualizarEnvios);

		var $form = $('#form-huellas');
		var args = $form.serialize();

		$.ajaxui('mostrar');
		$form.ajaxui('bloquear');
		$form.jsForm('disable');

		$.ajax({type: 'POST', url: $form.attr('action'), data: args + '&ajax=1', success: function (json) {
            $form.jsForm('enable');
            $form.ajaxui('desbloquear');
            $.ajaxui('ocultar');

            if(json == null){
                $seccion.find('.alert-error').text("No se obtuvo respuesta del controlador.").slideDown('slow');
            }
            else{
                if(json.error){
                    // mensaje de error en la seccion actual
                    $seccion.find('.alert-error').text(json.mensaje).slideDown('slow');
                }
                else{
                    // mensaje de exito en la nueva seccion
                    var mensaje = json['beanMom'].deMensaje + ' (Código de confirmación: ' + json['beanMom'].coRespuesta + ', ApplicationID: ' + json['beanMom'].applicationID + ')';
                    $seccion.next('section').find('.alert').text(mensaje);

                    var $container = $('#envios');
                    var $tabla = $container.find('table');
                    $tabla.find('tr:gt(0)').remove();

                    llenarTablaEnvios(json['listaRegistroAfis'], $tabla, true);

                    if($container.is(':hidden')){
                        $('div.lista-bottom header #lnk-toggle').trigger('click');
                    }
                    if(!$.browser.msie){
                        $container.scrollTop($tabla.height());
                    }
                }
            }

            idActualizarEnvios = setInterval(actualizarEnvios, tiempo_envios*1000);
        }
        , dataType: 'json', async: false});
	}

	return true;
}

function llenarTablaEnvios(lista, $tabla, add){
    $.each(lista, function(k,v){
        var fila = new Array(
            '<td>' + (k + 1) + '</td>'
            ,'<td>' + v.applicationId + '</td>'
            ,'<td>' + v.fecha + '</td>'
            ,'<td>' + v.hora + '</td>'
            ,'<td>' + (typeof v.deResultado == 'undefined' ? 'EN PROCESO <i class="icon-load"></i>' : v.deResultado) + '</td>'
            ,'<td>' + (v.resultado == 'A91' ? ' <a href="buscarCandidatosAfisN.do?applicationId='+v.applicationId+'" class="btn btn-small btn-danger lnk-ver-candidatos" title="Ver candidatos..."><i class="icon-user icon-white"></i></a>':'') + '</td>'
        );
        $tabla.append('<tr>' + fila.join('') + '</tr>');
        if(add && k == lista.length - 1){
            //console.log('entro al final');
            var $tr = $tabla.find('tr:eq('+(k+1)+')');
            $tr.css('color','#D54C4A');
            $tr.animate({color:'#000'},2000);
        }

        var nro_capturas = $tabla.find('tr').length - 1;
        if(add && nro_capturas >= max_capturas){
            $('.capa-central').ajaxui('bloquear');
            alert('Ud. ha llegado al nro. máximo de capturas permitidas.');
        }
    });
}

function reiniciarProceso() {
	//location.reload(true);
	$('a[href="unoToMuchos.do"]').click();
}


// funciones anteriores
function fn_capHuellaIzqN(formulario/*,url*/) {
   // var frm = $(objeto).parents("form").get(0);
   // var nomFormulario= frm.id;
	//var nomFormulario= $("#tab"+frm.id).tabs( "option", "selected" );

	var vRetorno = document.capturaSuport.CaptureFP();
	var vBMP = "";
	var vWSQ = "";
	var vANSI = "";
	var vPKSagem = "";
	//alert(formulario);

	var boolStatus;
	var cont = 0;
	var sRet = "0";

	if (vRetorno == "1") {
		//$("#IMG_IZQ").attr('src',url);
		vBMP = document.capturaSuport.getFileBMP();
		vWSQ = document.capturaSuport.getFileWSQ();
		vANSI = document.capturaSuport.getFileANSI();
		vPKSagem = document.capturaSuport.getFilePK();

		formulario.coCap_Izq.value=vRetorno;

		if(vBMP.toString() != ""){
			formulario.bmp_Izq.value=vBMP;
			//document.form1.bmp.value=vBMP;
		}
		if(vWSQ.toString() != ""){
			formulario.wsq_Izq.value=vWSQ;
			//document.form1.wsq.value=vWSQ;
		}
		if(vANSI.toString() != ""){
			formulario.ansi_Izq.value=vANSI;
			//document.form1.ansi.value=vANSI;
		}
		if(vPKSagem.toString() != ""){
			formulario.pkSagem_Izq.value=vPKSagem;
			//document.form1.pkSagem.value=vPKSagem;
		}


		//$(formulario).submit();

		vBMP = null;
		vWSQ = null;
		vANSI = null;
		vPKSagem = null;

		return true;

	}else{
		formulario.coCap_Izq.value="0";

		return false;
	}

}

 function fn_capHuellaDerN(formulario/*,url*/) {
	   // var frm = $(objeto).parents("form").get(0);
	   // var nomFormulario= frm.id;
		//var nomFormulario= $("#tab"+frm.id).tabs( "option", "selected" );

		var vRetorno = document.capturaSuport.CaptureFP();
		var vBMP = "";
		var vWSQ = "";
		var vANSI = "";
		var vPKSagem = "";
		//alert(formulario);
		if (vRetorno == "1") {
			//$("#IMG_DER").attr('src',url);


			vBMP = document.capturaSuport.getFileBMP();
			vWSQ = document.capturaSuport.getFileWSQ();
			vANSI = document.capturaSuport.getFileANSI();
			vPKSagem = document.capturaSuport.getFilePK();
			formulario.coCap_Der.value=vRetorno;

			if(vBMP.toString() != ""){
				formulario.bmp_Der.value=vBMP;
				//document.form1.bmp.value=vBMP;
			}
			if(vWSQ.toString() != ""){
				formulario.wsq_Der.value=vWSQ;
				//document.form1.wsq.value=vWSQ;
			}
			if(vANSI.toString() != ""){
				formulario.ansi_Der.value=vANSI;
				//document.form1.ansi.value=vANSI;
			}
			if(vPKSagem.toString() != ""){
				formulario.pkSagem_Der.value=vPKSagem;
				//document.form1.pkSagem.value=vPKSagem;
			}


			//$(formulario).submit();

			vBMP = null;
			vWSQ = null;
			vANSI = null;
			vPKSagem = null;

			return true;

		}else{
			formulario.coCap_Der.value="0";
			return false;
		}
}

//function fn_enviarHuellasAFIS(formulario){
//    $(formulario).submit();
//    return false;
//}