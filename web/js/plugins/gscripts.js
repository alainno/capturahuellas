/* 
 * gscripts.js v1.0
 * @autor: Alain - alain@gruposistemas.com
 */

// tabs: sirve para intercambiar contenido desde una lista
(function($){
	$.fn.gsTab = function(callback){
		var $tab_nuevo = $(this);
		
		var $tab_actual = $(this).parents('ul').find('a.actual');
		
		if($tab_actual != $tab_nuevo){
			$($tab_actual.attr('href')).hide();
			$tab_actual.removeClass('actual');
			$($tab_nuevo.attr('href')).show();
			$tab_nuevo.addClass('actual');
			
			if(typeof callback != 'undefined'){
				callback.call(this);
			}
		}
	}
})(jQuery);

// alerta de errores ajax
$.ajaxSetup({
	error:function(x,e){
		//alert(x.responseText);
		$("html").removeClass().html(x.responseText);
	}
});


// plugin para manipular el envio de formularios
/* formularios */
(function($){
	//
	var disablings = new Array();
	
	var methods = {
		enviar:function(options){
			options = options || {};
			//return this.each(function(){
			var target = $(this).attr('target');
			if(target != '' && target != '_self' && typeof target != 'undefined'){
				return true;
			}
			//console.log('no paso target adentro:' + target);
			var args = $(this).serialize().replace('%5B%5D', '[]');
			if(options['antes']) options['antes'].call(this);
			$.post($(this).attr('action'), args + '&ajax=1', function(data){
				if(options['despues'])options['despues'].call(this,data);
			}, 'json');
			return false;
		//});
		},
		//		bloquear : function(){
		//			return this.each(function(){
		//				var $capa = $(document.createElement('div'));
		//				$capa.addClass('capa cargando').css({
		//					'position':'absolute',
		//					'left':'0',
		//					'top':'0',
		//					'right':'0',
		//					'bottom':'0',
		//					'background':'#fff url(img/loader.png) no-repeat center',
		//					'opacity':'0.75'
		//				});
		//				this.css('position','relative').append($capa);
		//			});
		//		},
		//		desbloquear : function(){
		//			return this.each(function(){
		//				this.children('div.capa').remove();
		//			});
		//		},
		disable : function(){
			return this.each(function(){
				var form = $(this)[0];
				for(i = 0; i < form.elements.length; i++){
					disablings[i] = form.elements[i].disabled;
					form.elements[i].disabled = true;
				}
			});
		},
		enable : function(){
			return this.each(function(){
				var form = $(this)[0];
				for(i = 0; i < form.elements.length; i++){
					form.elements[i].disabled = disablings[i];
				}
			});
		}
	}

	$.fn.jsForm = function( method ) {
		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.formulario' );
		}
	}
	
})(jQuery);


// AJAX UI
(function($){
	var $loader = null;
	
	var methods = {
		crear:function(){
			$loader = $(document.createElement('div'));
			//return this.each(function(){
			$loader.attr('id', 'divLI').text('Ejecutando...').css({
				'position':'fixed',
				'display':'none',
				'z-index':'9999',
				'padding':'5px 15px',
				'background':'#D01F3C',
				'color':'#FFF',
				'font':'bold 12px Verdana, Arial, Helvetica, sans-serif',
				'left':'50%',
				'left': ($(window).width() - 138)/2 + 'px', 
				'top':0
			});
			$(window).resize(function(){
				$loader.css({
					'left': ($(window).width() - 138)/2 + 'px'
				});
			});
			$('body').append($loader);
		//});
		},
		mostrar:function(){
			if($loader == null) methods.crear.call(this);
			$loader.show();
		},
		ocultar:function(){
			$loader.hide();
		},
		bloquear:function(){
			return this.each(function(){
				var $capa = $(document.createElement('div'));
				$capa.addClass('cargando').css({
					'position':'absolute',
					'left':'0',
					'top':'0',
					'right':'0',
					'bottom':'0',
					'background':'#fff url(resources-1.0/img/loader.gif) no-repeat center',
					'opacity':'0.75'
				});
                var position = $(this).css('position');
				$(this).css('position',position=='absolute'?'absolute':'relative').append($capa);
			});
		},
		desbloquear:function(){
			return this.each(function(){
				$(this).children('div.cargando').remove();
                $(this).removeAttr('style');
			});
		}
	}
	
	$.fn.ajaxui = $.ajaxui = function(method){
		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.ajaxui' );
		}
	}
})(jQuery);

// animate colors
(function(d){d.each(["backgroundColor","borderBottomColor","borderLeftColor","borderRightColor","borderTopColor","color","outlineColor"],function(f,e){d.fx.step[e]=function(g){if(!g.colorInit){g.start=c(g.elem,e);g.end=b(g.end);g.colorInit=true}g.elem.style[e]="rgb("+[Math.max(Math.min(parseInt((g.pos*(g.end[0]-g.start[0]))+g.start[0]),255),0),Math.max(Math.min(parseInt((g.pos*(g.end[1]-g.start[1]))+g.start[1]),255),0),Math.max(Math.min(parseInt((g.pos*(g.end[2]-g.start[2]))+g.start[2]),255),0)].join(",")+")"}});function b(f){var e;if(f&&f.constructor==Array&&f.length==3){return f}if(e=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(f)){return[parseInt(e[1]),parseInt(e[2]),parseInt(e[3])]}if(e=/rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(f)){return[parseFloat(e[1])*2.55,parseFloat(e[2])*2.55,parseFloat(e[3])*2.55]}if(e=/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(f)){return[parseInt(e[1],16),parseInt(e[2],16),parseInt(e[3],16)]}if(e=/#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(f)){return[parseInt(e[1]+e[1],16),parseInt(e[2]+e[2],16),parseInt(e[3]+e[3],16)]}if(e=/rgba\(0, 0, 0, 0\)/.exec(f)){return a.transparent}return a[d.trim(f).toLowerCase()]}function c(g,e){var f;do{f=d.css(g,e);if(f!=""&&f!="transparent"||d.nodeName(g,"body")){break}e="backgroundColor"}while(g=g.parentNode);return b(f)}var a={aqua:[0,255,255],azure:[240,255,255],beige:[245,245,220],black:[0,0,0],blue:[0,0,255],brown:[165,42,42],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgrey:[169,169,169],darkgreen:[0,100,0],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkviolet:[148,0,211],fuchsia:[255,0,255],gold:[255,215,0],green:[0,128,0],indigo:[75,0,130],khaki:[240,230,140],lightblue:[173,216,230],lightcyan:[224,255,255],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightyellow:[255,255,224],lime:[0,255,0],magenta:[255,0,255],maroon:[128,0,0],navy:[0,0,128],olive:[128,128,0],orange:[255,165,0],pink:[255,192,203],purple:[128,0,128],violet:[128,0,128],red:[255,0,0],silver:[192,192,192],white:[255,255,255],yellow:[255,255,0],transparent:[255,255,255]}})(jQuery);