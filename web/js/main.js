/*
 * Para errores a ser mostrados en internet explorer
 */
if (typeof(console) == 'undefined') {
    console = {
        log: function (message) {
            //alert(message);
        }
    }
}

$(function () {
    Aplicacion = new Application('#main-content', 'noAjax',
        {
            401: function () {
                document.location = '';
            }
        },
        function () {
            $("#errorButton").hide();

//
//            alert('hi5');
//
//            console.log(this);
        },
        function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 401) {
                $('#debug-content').contents().find('html').html(jqXHR.responseText);
                $("#errorModal").modal('show');
                $("#errorButton").show();
            }
        }
    );
    $("#errorButton").hide();
    Aplicacion.InicializarAcciones();



});

function fn_newQuery(obj){
       obj.style.display="none";
       $("#fichaVistaDetalle, #msgHuella").hide();
}

function fn_login(formulario) {
        $(formulario).submit();
}
function fn_capturar(formulario) {
   // var frm = $(objeto).parents("form").get(0);
   // var nomFormulario= frm.id;
    //var nomFormulario= $("#tab"+frm.id).tabs( "option", "selected" );

//    var pUsuario = formulario.user.value;
//
//    if(pUsuario == null||pUsuario==""){
//        alert("Debe ingresar el codigo del usuario");
//        return false;
//    }

    var vRetorno = document.capturaSuport.CaptureFP();
    var vBMP = "";
    var vWSQ = "";
    var vANSI = "";
    var vPKSagem = "";
    //alert(formulario);
    if (vRetorno == "1") {
        vBMP = document.capturaSuport.getFileBMP();
        vWSQ = document.capturaSuport.getFileWSQ();
        vANSI = document.capturaSuport.getFileANSI();
        vPKSagem = document.capturaSuport.getFilePK();
        if(vBMP.toString() != ""){
            formulario.bmp.value=vBMP;
            //document.form1.bmp.value=vBMP;
        }
        if(vWSQ.toString() != ""){
            formulario.wsq.value=vWSQ;
            //document.form1.wsq.value=vWSQ;
        }
        if(vANSI.toString() != ""){
            formulario.ansi.value=vANSI;
            //document.form1.ansi.value=vANSI;
        }
        if(vPKSagem.toString() != ""){
            formulario.pkSagem.value=vPKSagem;
            //document.form1.pkSagem.value=vPKSagem;
        }

        if(formulario.bmp.value.length > 0 || formulario.wsq.value.length> 0 || formulario.ansi.value.length > 0 || formulario.pkSagem.value.length > 0){
            $('#msgHuella').show();
            $(formulario).submit();
        }
        else{
            alert("No se ha capturado la huella, revisar el funcionamiento del lector biométrico")
        }

        vBMP = null;
        vWSQ = null;
        vANSI = null;
        vPKSagem = null;
    }else{
        alert("El lector biométrico no ha devuelto ningun resultado")
    }
}

function fn_consultarOneToOne(ruta, formulario){
    $("#fichaVistaDetalle, #msgHuella").hide();
    var oForm = formulario.parentNode;
    var numeroDNI = "";

    numeroDNI = oForm.txtNumeroDNI.value;
    document.frmHuellas.dniCiudadano.value = oForm.txtNumeroDNI.value;

    if(fu_numeroDNI(numeroDNI)==true){
        var parametros={
            numeroDNI:numeroDNI
        }
        $.post(ruta+"/verificarDNI.do",parametros, function(dataXML){
            if(dataXML=="OK"){
                fn_capturar(document.frmHuellas);

                //$("#frmOneToOne").submit();

                var $form = $('#frmOneToOne');
                var action = $form.attr('action');
                var index = action.indexOf('#');
                var target = action.substr(index);
                var args = $form.serialize();

                $.post(ruta + '/' + action, args, function(data){
                   $(target).show().html(data);
                });

                return false;
            }else{
                alert("El DNI : "+numeroDNI + " , no existe en la Base de Datos");
                oForm.txtNumeroDNI.value="";
                oForm.txtNumeroDNI.focus() ;
                return false;
            }

        },'HTML');
    }
    return false;
}

function fn_capHuellaIzq(formulario,url) {
   // var frm = $(objeto).parents("form").get(0);
   // var nomFormulario= frm.id;
    //var nomFormulario= $("#tab"+frm.id).tabs( "option", "selected" );

    var vRetorno = document.capturaSuport.CaptureFP();
    var vBMP = "";
    var vWSQ = "";
    var vANSI = "";
    var vPKSagem = "";
    //alert(formulario);

    var boolStatus;
    var cont = 0;
    var sRet = "0";

    if (vRetorno == "1") {
        $("#IMG_IZQ").attr('src',url);
        vBMP = document.capturaSuport.getFileBMP();
        vWSQ = document.capturaSuport.getFileWSQ();
        vANSI = document.capturaSuport.getFileANSI();
        vPKSagem = document.capturaSuport.getFilePK();

        formulario.coCap_Izq.value=vRetorno;

        if(vBMP.toString() != ""){
            formulario.bmp_Izq.value=vBMP;
            //document.form1.bmp.value=vBMP;
        }
        if(vWSQ.toString() != ""){
            formulario.wsq_Izq.value=vWSQ;
            //document.form1.wsq.value=vWSQ;
        }
        if(vANSI.toString() != ""){
            formulario.ansi_Izq.value=vANSI;
            //document.form1.ansi.value=vANSI;
        }
        if(vPKSagem.toString() != ""){
            formulario.pkSagem_Izq.value=vPKSagem;
            //document.form1.pkSagem.value=vPKSagem;
        }


        //$(formulario).submit();



        vBMP = null;
        vWSQ = null;
        vANSI = null;
        vPKSagem = null;
    }else{
        formulario.coCap_Izq.value="0";
    }

}

 function fn_capHuellaDer(formulario,url) {
       // var frm = $(objeto).parents("form").get(0);
       // var nomFormulario= frm.id;
        //var nomFormulario= $("#tab"+frm.id).tabs( "option", "selected" );

        var vRetorno = document.capturaSuport.CaptureFP();
        var vBMP = "";
        var vWSQ = "";
        var vANSI = "";
        var vPKSagem = "";
        //alert(formulario);
        if (vRetorno == "1") {
            $("#IMG_DER").attr('src',url);


            vBMP = document.capturaSuport.getFileBMP();
            vWSQ = document.capturaSuport.getFileWSQ();
            vANSI = document.capturaSuport.getFileANSI();
            vPKSagem = document.capturaSuport.getFilePK();
            formulario.coCap_Der.value=vRetorno;

            if(vBMP.toString() != ""){
                formulario.bmp_Der.value=vBMP;
                //document.form1.bmp.value=vBMP;
            }
            if(vWSQ.toString() != ""){
                formulario.wsq_Der.value=vWSQ;
                //document.form1.wsq.value=vWSQ;
            }
            if(vANSI.toString() != ""){
                formulario.ansi_Der.value=vANSI;
                //document.form1.ansi.value=vANSI;
            }
            if(vPKSagem.toString() != ""){
                formulario.pkSagem_Der.value=vPKSagem;
                //document.form1.pkSagem.value=vPKSagem;
            }


            //$(formulario).submit();

            vBMP = null;
            vWSQ = null;
            vANSI = null;
            vPKSagem = null;
        }else{
            formulario.coCap_Der.value="0";
        }
}

function fn_enviarHuellasAFIS(formulario){
    $(formulario).submit();

    return false;
}

function fu_numeroDNI(numeroDNI){
    if(numeroDNI == null||numeroDNI==""){
        alert("El numero de DNI no puede ser vacio");
        return false;
    }else if(numeroDNI.length != 8){
        alert("La cantidad de digitos del DNI debe ser 8");
        return false;
    }
    return true;
}

$(document).ready(function(){

    $('#frmOneToOne').live('submit', function(e){
        e.preventDefault();
        $("#btnBuscarDNI").click();
    });


});